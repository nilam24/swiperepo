package com.nilam.swipetest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;
import java.util.List;

public class AdapterRecycle extends RecyclerView.Adapter<AdapterRecycle.viewHolderUser> {
    List<User> list=new ArrayList<>();
    Boolean b;
    void setList(List<User> list, Boolean b){
        this.list=list;
        this.b=b;

    }

    @NonNull
    @Override
    public viewHolderUser onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,null);


        return new viewHolderUser(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderUser holder, int position) {
        User user=list.get(position);
        holder.nam1.setText(user.name);
        holder.nam2.setText(user.mobile);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class viewHolderUser extends RecyclerView.ViewHolder{
        TextView btn,nam1,nam2;
        SwipeLayout swipeLayout;
        public viewHolderUser(@NonNull View view) {
            super(view);
            btn = view.findViewById(R.id.del);
            nam1 = view.findViewById(R.id.txt1);
            nam2 = view.findViewById(R.id.txt2);
            swipeLayout = view.findViewById(R.id.swipe1);


        }
    }
}
