package com.nilam.swipetest;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.ArraySwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemAdapterMangerImpl;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.daimajia.swipe.interfaces.SwipeItemMangerInterface;
import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;
import java.util.List;

interface SwipeTask{
    void setSwipe(int position);
}


public class AdapterSwipe extends ArraySwipeAdapter<User> implements SwipeItemMangerInterface, SwipeAdapterInterface {
   // SwipeItemMangerImpl swipeItemManger=new SwipeItemManagerImpl(this);
    SwipeItemMangerImpl itemManger=new SwipeItemAdapterMangerImpl(this);
    SwipeTask swipeTask;
    List<User> list=new ArrayList<>();
    Boolean b;
    public AdapterSwipe(@NonNull Context context, int resource) {
        super(context, resource);
    }

    void setList(List<User>list,Boolean b){
        this.list=list;
        this.b=b;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,null);

        swipeTask=new MainActivity();

       // ViewHolder holder= (ViewHolder) view.getTag();
        TextView btn,nam1,nam2;
        SwipeLayout swipeLayout;
        boolean isConverview=convertView==null;
        if(isConverview)
        itemManger.initialize(view,position);
        else
            itemManger.updateConvertView(view,position);
       // itemManger.bindView(view,position);
       // itemManger.setMode(Attributes.Mode.Single);

        btn = view.findViewById(R.id.del);
        nam1 = view.findViewById(R.id.txt1);
        nam2 = view.findViewById(R.id.txt2);
        swipeLayout = view.findViewById(R.id.swipe1);
        User user=list.get(position);
        nam1.setText(user.name);
        nam2.setText(user.mobile);
        if(b==true){
            btn.setText("Non Voted");
        }
        else if(b==false){
            btn.setText("Voted");
        }
        Log.e("......."+user.name,""+list.size());

        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos= (int) view.getTag();
                swipeTask.setSwipe(pos);
                list.remove(position);
                notifyDataSetChanged();
            }
        });

//        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
//            @Override
//            public void onStartOpen(SwipeLayout layout) {
//              layout.close();
//            }
//
//            @Override
//            public void onOpen(SwipeLayout layout) {
//
//            }
//
//            @Override
//            public void onStartClose(SwipeLayout layout) {
//
//            }
//
//            @Override
//            public void onClose(SwipeLayout layout) {
//
//            }
//
//            @Override
//            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
//
//            }
//
//            @Override
//            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
//
//            }
//        });

        return view;
    }

    @Override
    public void openItem(int position) {
     itemManger.openItem(position);
        //super.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        itemManger.closeItem(position);
        //super.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        itemManger.closeAllExcept(layout);
        //super.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        itemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return itemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return itemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        itemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return itemManger.isOpen(position);
    }

    @Override
    public Attributes.Mode getMode() {
        return super.getMode();
    }

    @Override
    public void setMode(Attributes.Mode mode) {
        super.setMode(mode);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe1;
    }

    static class ViewHolder{
    TextView btn,nam1,nam2;
    SwipeLayout swipeLayout;
    ViewHolder(View itemview) {
        btn = itemview.findViewById(R.id.del);
        nam1 = itemview.findViewById(R.id.txt1);
        nam2 = itemview.findViewById(R.id.txt2);
        swipeLayout = itemview.findViewById(R.id.swipe1);
    }

}}
