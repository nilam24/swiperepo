package com.nilam.swipetest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.browse.MediaBrowser;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeTask {

    ListView listView;
    RecyclerView recyclerView;
    ArrayList<User>list=new ArrayList<>();
    AdapterSwipe adapterSwipe;
    AdapterRecycle adapterRecycle;
    Boolean status=true;
    int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.list_swipe);
        recyclerView=findViewById(R.id.recycle);
        list=setList();
        adapterSwipe=new AdapterSwipe(getApplicationContext(),0);
        adapterRecycle=new AdapterRecycle();
       // adapterSwipe.setList(list,status);
        adapterRecycle.setList(list,status);
        
        LinearLayoutManager layoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterRecycle);
        adapterRecycle.notifyDataSetChanged();
        listView.setAdapter(adapterSwipe);



        final ItemTouchHelper.SimpleCallback simpleCallback=new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if(!list.isEmpty()&& !(viewHolder.getAdapterPosition()<0)) {
                    list.remove(viewHolder.getAdapterPosition());
                    sendWhatsapp(" "," ","hiiiiiiii");
                    count++;
                    Toast.makeText(getApplicationContext(),"count= "+count,Toast.LENGTH_LONG).show();
                }


            }
        };
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerView);

    }

   ArrayList<User> setList(){

        User user=new User("name1","1234567");
        User user2=new User("name2","1234567");
        User user3=new User("name3","1234567");
        User user4=new User("name4","1234567");
        User user5=new User("name5","1234567");
        User user6=new User("name6","1234567");
        list.add(user);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);
        list.add(user6);
        return list;

    }

    @Override
    public void setSwipe(int position) {
        String nam=list.get(position).getName();
        Log.e("namee..",""+nam);
        list.remove(position);
        adapterSwipe.notifyDataSetChanged();
    }

    private void sendWhatsapp(String number,String number2,String msg){
        PackageManager packageManager=getPackageManager();
        Uri uri1=Uri.parse("smsTo:"+number);
        Uri uri=null,uri2= null;
        try {
           // uri = Uri.parse("https://api.whatsapp.com/send?phone="+number +"&text=" + URLEncoder.encode(msg,"UTF-8"));
            //uri2 = Uri.parse("https://api.whatsapp.com/send?phone="+number2 +"&text="+ URLEncoder.encode(msg,"UTF-8"));
            uri=Uri.parse("https://chat.whatsapp.com/<group-link>");
        }   //catch (UnsupportedEncodingException e) {
        catch (Exception e){
            e.printStackTrace();
        }
        Intent intent=new Intent(Intent.ACTION_VIEW);

        try {
            PackageInfo packageInfo=packageManager.getPackageInfo("com.whatsapp",PackageManager.GET_META_DATA);
            if(packageInfo!=null)
              intent.setPackage("com.whatsapp");
              intent.setType("text/plain");
              intent.setData(uri);
             // intent.setData(uri2);
             // intent.putExtra("jid","916201012928"+"@s.whatsapp.net");
              intent.putExtra(Intent.EXTRA_TEXT,"hi..");
              startActivity(intent);
             // startActivity(Intent.createChooser(intent,"share text"));
            } catch (PackageManager.NameNotFoundException e) {
                  e.printStackTrace();
              }
        }
}
